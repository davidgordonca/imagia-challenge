# map user and group from host to container
CONTAINER_USERNAME = ubuntu
CONTAINER_GROUPNAME = ubuntu
HOMEDIR = /home/$(CONTAINER_USERNAME)
GROUP_ID = $(shell id -g)
USER_ID = $(shell id -u)
CREATE_USER_COMMAND = \
  groupadd -f -g $(GROUP_ID) $(CONTAINER_GROUPNAME) && \
  useradd -u $(USER_ID) -g $(CONTAINER_GROUPNAME) $(CONTAINER_USERNAME) && \
  mkdir -p $(HOMEDIR) &&

# map SSH identity from host to container
DOCKER_SSH_IDENTITY ?= ~/.ssh/id_rsa
DOCKER_SSH_KNOWN_HOSTS ?= ~/.ssh/known_hosts
ADD_SSH_ACCESS_COMMAND = \
  mkdir -p $(HOMEDIR)/.ssh && \
  test -e /var/tmp/id && cp /var/tmp/id $(HOMEDIR)/.ssh/id_rsa ; \
  test -e /var/tmp/known_hosts && cp /var/tmp/known_hosts $(HOMEDIR)/.ssh/known_hosts ; \
  test -e $(HOMEDIR)/.ssh/id_rsa && chmod 600 $(HOMEDIR)/.ssh/id_rsa ;

# utility commands
AUTHORIZE_HOME_DIR_COMMAND = chown -R $(CONTAINER_USERNAME):$(CONTAINER_GROUPNAME) $(HOMEDIR) &&
EXECUTE_AS = sudo -u $(CONTAINER_USERNAME) HOME=$(HOMEDIR)

# If the first argument is one of the supported commands...
SUPPORTED_COMMANDS := clang-3.5 gcc-4.4 gcc-4.9
SUPPORTS_MAKE_ARGS := $(findstring $(firstword $(MAKECMDGOALS)), $(SUPPORTED_COMMANDS))
ifneq "$(SUPPORTS_MAKE_ARGS)" ""
  # use the rest as arguments for the command
  COMMAND_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(COMMAND_ARGS):;@:)
endif

clang-3.5:
	@docker run -ti --rm=true \
		-v `pwd`:/usr/local \
		-v $(DOCKER_SSH_IDENTITY):/var/tmp/id \
		-v $(DOCKER_SSH_KNOWN_HOSTS):/var/tmp/known_hosts \
		davidgordonca/docker-clang-3.5 bash -ci '\
			$(CREATE_USER_COMMAND) \
			$(ADD_SSH_ACCESS_COMMAND) \
			$(AUTHORIZE_HOME_DIR_COMMAND) \
			$(EXECUTE_AS) $(COMMAND_ARGS)'

gcc-4.4:
	@docker run -ti --rm=true \
                -v `pwd`:/usr/local \
                -v $(DOCKER_SSH_IDENTITY):/var/tmp/id \
                -v $(DOCKER_SSH_KNOWN_HOSTS):/var/tmp/known_hosts \
                davidgordonca/docker-gcc-4.4 bash -ci '\
                        $(CREATE_USER_COMMAND) \
                        $(ADD_SSH_ACCESS_COMMAND) \
                        $(AUTHORIZE_HOME_DIR_COMMAND) \
                        $(EXECUTE_AS) $(COMMAND_ARGS)'

gcc-4.9:
	@docker run -ti --rm=true \
                -v `pwd`:/usr/local \
                -v $(DOCKER_SSH_IDENTITY):/var/tmp/id \
                -v $(DOCKER_SSH_KNOWN_HOSTS):/var/tmp/known_hosts \
                davidgordonca/docker-gcc-4.9 bash -ci '\
                        $(CREATE_USER_COMMAND) \
                        $(ADD_SSH_ACCESS_COMMAND) \
                        $(AUTHORIZE_HOME_DIR_COMMAND) \
                        $(EXECUTE_AS) $(COMMAND_ARGS)'

